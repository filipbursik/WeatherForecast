package com.inventi.weatherforecast.model.di;

import com.inventi.weatherforecast.activities.MainActivity;
import com.inventi.weatherforecast.model.activitymodel.CurrentWeatherModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {MainModule.class})
public interface DaggerGraphComponent {
    void inject(MainActivity mainActivity);

    static final class Initializer {
        private Initializer() {
        }

        public static DaggerGraphComponent init(DaggerApplication app) {
            return DaggerDaggerGraphComponent.builder()
                                             .mainModule(new MainModule(app))
                                             .build();
        }
    }
}
