package com.inventi.weatherforecast.model.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import com.inventi.weatherforecast.fragments.CurrentWeatherFragment;
import com.inventi.weatherforecast.fragments.FutureWeatherFragment;

/**
 * Created by filas on 22.10.2016.
 * Custom adapter for ViewPager(in ViewPager are two fragments -  future and current weather)
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private int num;
    private View view;

    public ViewPagerAdapter(FragmentManager fm, int num, View view) {
        super(fm);
        this.view = view;
        this.num = num;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Fragment tab1 = new CurrentWeatherFragment();
                return tab1;
            case 1:
                Fragment tab2 = new FutureWeatherFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return num;
    }
}
