package com.inventi.weatherforecast.service.impl;

import android.content.Context;

import com.inventi.weatherforecast.model.entities.Forecast;
import com.inventi.weatherforecast.service.api.FromToFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * Created by filas on 12.11.2016.
 */

public class FromToFileImpl implements FromToFile {

    public List<String> readFileList(Context c) {
        return null;
    }

    public Forecast readFileForecast(Context c) {
        return null;
    }

    public void writeFile(List<String> l, Context c) {
        try {
            FileOutputStream fout = new FileOutputStream(new File(c.getFilesDir(), "list.wf"));
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(l);
            oos.close();
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeFile(Forecast f, Context c) {
        try {
            FileOutputStream fout = new FileOutputStream(new File(c.getFilesDir(), "list.wf"));
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(f);
            oos.close();
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
