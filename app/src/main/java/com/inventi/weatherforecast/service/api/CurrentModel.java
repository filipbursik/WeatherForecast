package com.inventi.weatherforecast.service.api;

/**
 * Created by filas on 12.11.2016.
 */
public interface CurrentModel {

    public String requestCurrent(String city);
    public String requestFuture(String city, int days);

}
