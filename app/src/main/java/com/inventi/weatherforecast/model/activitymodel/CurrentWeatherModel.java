package com.inventi.weatherforecast.model.activitymodel;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.SubMenu;
import android.widget.EditText;
import android.widget.TextView;

import com.inventi.weatherforecast.R;
import com.inventi.weatherforecast.activities.MainActivity;
import com.inventi.weatherforecast.model.adapters.ViewPagerAdapter;
import com.inventi.weatherforecast.model.di.DaggerApplication;
import com.inventi.weatherforecast.service.api.CurrentModel;
import com.inventi.weatherforecast.service.api.FromToFile;
import com.inventi.weatherforecast.utilities.Requests;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by filas on 12.11.2016.
 */

public class CurrentWeatherModel {

    // Injects
    @Inject CurrentModel mCurrentModel;
    @Inject FromToFile sFromToFile;

    // Variables
    private Context context;
    private AppCompatActivity act;
    private SubMenu topChannelMenu;
    private String name;

    // Constants
    private final int NUMBER_OF_DAYS = 10; // Number of days to forecast

    // Objects
    private Requests rqs = new Requests();

    // ArrayLists
    private List<String> list = new ArrayList<>();

    public CurrentWeatherModel(AppCompatActivity act, Context context) {
        this.act = act;
        this.context = context;

        DaggerApplication.component().inject((MainActivity) act);
        System.out.println("sd");
    }

    /**
     * Add city to List of cities
     */
    public void addCity(String name){
        this.name = name;

        topChannelMenu.add(name);
        list.add(name);

        // Send requests to weather API
        mCurrentModel.requestCurrent(name);
        mCurrentModel.requestFuture(name, NUMBER_OF_DAYS);

        // Write data list of cities to file
        sFromToFile.writeFile(list, context);
    }

    /**
     * Delete all cities from NavigationDrawer
     */
    public void clearNav() {
        // Clear navigation drawer
        topChannelMenu.clear();

        // Delete the file
        new File(context.getFilesDir(), "list.wf").delete();
    }

    /**
     * Save current city to variable
     * @param s name of city
     */
    public void navClicked(String s) {
        name = s;
    }

    /**
     * Setup pager
     * @param pager
     */
    public void setupPager(ViewPager pager){
        final ViewPagerAdapter adapter = new ViewPagerAdapter
                (act.getSupportFragmentManager(), 2, act.getWindow().getDecorView().getRootView());
        pager.setAdapter(adapter);
    }

    /**
     * Setup NavigationDrawer
     * @param navView
     */
    public void confDrawer(NavigationView navView) {
        Menu m = navView.getMenu();
        topChannelMenu = m.addSubMenu("Cities");
    }
}
