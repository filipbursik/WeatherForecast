package com.inventi.weatherforecast.service.api;

import android.content.Context;

import com.inventi.weatherforecast.model.entities.Forecast;

import java.util.List;

/**
 * Created by filas on 12.11.2016.
 */

public interface FromToFile {
    public List<String> readFileList(Context c);
    public Forecast readFileForecast(Context c);

    public void writeFile(List<String> a, Context c);
    public void writeFile(Forecast f, Context c);
}
