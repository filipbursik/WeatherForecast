package com.inventi.weatherforecast.service.impl;

import android.util.Log;

/**
 * Created by filas on 12.11.2016.
 */

public class CurrentModelImpl implements com.inventi.weatherforecast.service.api.CurrentModel {

    @Override
    public String requestCurrent(String city) {
        Log.e("Logger", "Sending request for current weather");
        return null;
    }

    @Override
    public String requestFuture(String city, int days) {
        Log.e("Logger", "Sending request for future");
        return null;
    }
}
