package com.inventi.weatherforecast.model.di;

import android.app.Application;

import com.inventi.weatherforecast.service.api.CurrentModel;
import com.inventi.weatherforecast.service.api.FromToFile;
import com.inventi.weatherforecast.service.impl.CurrentModelImpl;
import com.inventi.weatherforecast.service.impl.FromToFileImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {
    DaggerApplication app;

    public MainModule(DaggerApplication application) {
        app = application;
    }

    @Provides
    @Singleton
    protected Application provideApplication() {
        return app;
    }

    @Provides
    @Singleton
    FromToFile provideFileService() {
        return new FromToFileImpl();
    }

    @Provides
    @Singleton
    CurrentModel provideWeatherService() {
        return new CurrentModelImpl();
    }
}
