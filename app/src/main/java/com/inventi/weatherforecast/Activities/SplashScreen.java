package com.inventi.weatherforecast.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.inventi.weatherforecast.R;

/**
 * Splash screen
 * Only function is, that shows image, when the app started, for 3 seconds
 * Created by filas on 22.10.2016.
 */

public class SplashScreen extends Activity {

    private static int TIMER = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Handler for making the 3000 ms delay
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, TIMER);
    }

}
