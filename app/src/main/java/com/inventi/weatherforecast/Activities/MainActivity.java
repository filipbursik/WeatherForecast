package com.inventi.weatherforecast.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.inventi.weatherforecast.model.activitymodel.CurrentWeatherModel;
import com.inventi.weatherforecast.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Main activity of the app
 */

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // Bind views
    @BindView(R.id.view_pager) ViewPager pager;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navView;

    // Variables
    private CurrentWeatherModel mCurrentWeather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navView.setNavigationItemSelectedListener(this);
    }

    /**
     * Method called after onCreate becose then app context is created
     */
    @Override
    public void onStart(){
        super.onStart();

        mCurrentWeather = new CurrentWeatherModel(this, getBaseContext());

        mCurrentWeather.setupPager(pager);
        mCurrentWeather.confDrawer(navView);
    }

    /**
     * Overrided onBackPressed, becose of navigation drawer
     */
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        mCurrentWeather.navClicked(item.toString());

        return true;
    }

    /**
     * On click for floating button, shows dialog alert to enter name of the city
     */
    @Optional
    @OnClick(R.id.add)
    public void onFloating() {
        Log.e("Logger", "Clicked on add");

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        // Make EditText to DialogAlert
        final EditText input = new EditText(MainActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);

        dialogBuilder.setView(input);
        dialogBuilder.setTitle("Add city");
        dialogBuilder.setMessage("Enter name of city");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Log.e("Logger", input.toString());
                mCurrentWeather.addCity((input.getText().toString()));
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    /**
     * On click for delete button, delete all cities from navigation drawer
     */
    @Optional
    @OnClick(R.id.delete)
    public void delete() {
        Log.e("Logger", "Clicked on delete");
        Toast.makeText(getApplicationContext(), "All cities from navigation drawer have been deleted!", Toast.LENGTH_SHORT).show();

        mCurrentWeather.clearNav();
    }
}
